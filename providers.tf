terraform {
  backend "s3" {
    bucket = "feb-23-tf-state-bucket1"
    key    = "eks-deployment/terraform.tfstate"
    region = "us-east-1"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.10.0"
    }
  }
}

provider "aws" {
    region = "us-east-1"
}

